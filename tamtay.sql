-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 05, 2014 at 10:30 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tamtay`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `idcategory` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`idcategory`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`idcategory`, `name`) VALUES
(1, 'kinh te'),
(2, 'Chinh tri');

-- --------------------------------------------------------

--
-- Table structure for table `chucvu`
--

CREATE TABLE IF NOT EXISTS `chucvu` (
  `idchucvu` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`idchucvu`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `chucvu`
--

INSERT INTO `chucvu` (`idchucvu`, `name`) VALUES
(0, 'Nhân viên'),
(1, 'giam doc'),
(2, 'CTO'),
(3, 'Managermen'),
(4, 'Leader');

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE IF NOT EXISTS `group` (
  `idgroup` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`idgroup`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='cần giữ nguyên, do lquan cấu hình phân quyền trong code' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`idgroup`, `name`, `status`) VALUES
(0, 'admin', '''password'',''birthday'',''start_job'',''end_job'',''avatar'''),
(1, 'member', '{"idlevel":0,"phong":"1","parent":0,"iduser":"1","username":"1","email":"1","phone":0,"fullname":0,"birthday":"1","address":"1","start_job":0,"end_job":0,"":null}'),
(2, 'nhập thông báo', '{"idlevel":0,"phong":"1","parent":0,"iduser":0,"username":"1","email":"1","phone":0,"fullname":0,"birthday":0,"address":0,"start_job":0,"end_job":0,"":null}'),
(3, 'nhập info', '{"idlevel":0,"phong":0,"parent":0,"iduser":0,"username":0,"email":0,"phone":0,"fullname":0,"birthday":0,"address":0,"start_job":0,"end_job":0}');

-- --------------------------------------------------------

--
-- Table structure for table `group_user`
--

CREATE TABLE IF NOT EXISTS `group_user` (
  `iduser` int(11) NOT NULL,
  `idgroup` int(11) NOT NULL,
  PRIMARY KEY (`idgroup`,`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_user`
--

INSERT INTO `group_user` (`iduser`, `idgroup`) VALUES
(61, 0),
(126, 1),
(127, 1),
(124, 2),
(125, 2),
(121, 3),
(126, 3);

-- --------------------------------------------------------

--
-- Table structure for table `key`
--

CREATE TABLE IF NOT EXISTS `key` (
  `idkey` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`idkey`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `key`
--

INSERT INTO `key` (`idkey`, `name`) VALUES
(1, 'avatar'),
(9, 'trangnt'),
(31, 'new atribute');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `idlevel` int(11) NOT NULL AUTO_INCREMENT COMMENT '111: đang trong trạng thái chờ, user nào có idlevel này thì chưa thuộc về phòng nào',
  `phong` varchar(20) NOT NULL,
  `parent` int(11) NOT NULL COMMENT '0: giám đốc, không d�',
  PRIMARY KEY (`idlevel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=112 ;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`idlevel`, `phong`, `parent`) VALUES
(1, 'giám đốc', 0),
(3, 'php', 1),
(4, 'SA', 1),
(5, 'game', 1),
(111, 'pending', 0);

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `idlog` int(11) NOT NULL AUTO_INCREMENT,
  `idusers` int(11) NOT NULL,
  `detail` longtext,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idlog`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=112 ;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`idlog`, `idusers`, `detail`, `create_time`) VALUES
(1, 2, 'noi dung', '2014-08-10 17:00:00'),
(2, 2, 'noi dung', '2014-08-10 17:00:00'),
(3, 62, '-nghỉ đẻ sinh 3 con', '2014-08-18 17:00:00'),
(4, 63, 'Thông tin cũ: idusers:63<br/>username:thanh ld<br/>email:thanhld@gmail.com<br/>password:789<br/>phone:0168998829123<br/>idroom:1<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>fullname:thanh le dinh<br/>birthday:0000-00-00<br/>address:thanh hoa hh<br/>room:phong kinh doanh<br/>Thông tin mới: username:thanh ldjh<br/>address:thanh hoa hh<br/>birthday:--0000-00-00<br/>email:thanhld@gmail.com<br/>password:789<br/>fullname:thanh le dinh<br/>phone:0168998829123<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>idroom:1<br/>', '2014-08-21 17:00:00'),
(5, 63, 'Thông tin cũ: idusers:63	username:thanh ldjh	email:thanhld@gmail.com	password:789	phone:0168998829123	idroom:1	start_job:0000-00-00	end_job:0000-00-00	fullname:thanh le dinh	birthday:0000-00-00	address:thanh hoa hh	room:phong kinh doanh	Thông tin mới: username:thanh	address:thanh hoa hh	birthday:--0000-00-00	email:thanhld@gmail.com	password:7891	fullname:thanh le dinh	phone:0168998829123	start_job:0000-00-00	end_job:0000-00-00	idroom:1	', '2014-08-21 17:00:00'),
(6, 107, 'Thông tin cũ: idusers:107	username:giamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:giamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:	idroom:1	', '2014-08-25 17:00:00'),
(7, 107, 'Thông tin cũ: idusers:107	username:giamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:giamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:4	', '2014-08-25 17:00:00'),
(8, 107, 'Thông tin cũ: idusers:107	username:giamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:3	', '2014-08-25 17:00:00'),
(9, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(10, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(11, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(12, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(13, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(14, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(15, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(16, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:4	', '2014-08-25 17:00:00'),
(17, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:4	', '2014-08-25 17:00:00'),
(18, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(19, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(20, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(21, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(22, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(23, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(24, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(25, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(26, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(27, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(28, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(29, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(30, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(31, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(32, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(33, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:mygiamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-25 17:00:00'),
(34, 107, 'Thông tin cũ: idusers:107	username:mygiamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:giamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-26 17:00:00'),
(35, 107, 'Thông tin cũ: idusers:107	username:giamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:giamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-27 17:00:00'),
(36, 107, 'Thông tin cũ: idusers:107	username:giamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:giamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-27 17:00:00'),
(37, 107, 'Thông tin cũ: idusers:107	username:giamdoc	email:giamdoc@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:giam doc	birthday:0000-00-00	address:ha noi	<br/>Thông tin mới: username:giamdoc	address:ha noi	birthday:--0000-00-00	email:giamdoc@gmail.com	password:123456	fullname:giam doc	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:3	', '2014-08-27 17:00:00'),
(38, 108, 'Thông tin cũ: idusers:108	username:noname	email:no@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:	fullname:nn	birthday:0000-00-00	address:	<br/>Thông tin mới: username:noname	address:	birthday:--0000-00-00	email:no@gmail.com	password:123456	fullname:nn	phone:01934719893	start_job:2014-08-08	end_job:	idroom:5	', '2014-08-27 17:00:00'),
(39, 108, 'Thông tin cũ: idusers:108	username:noname	email:no@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:nn	birthday:0000-00-00	address:	<br/>Thông tin mới: username:noname	address:	birthday:--0000-00-00	email:no@gmail.com	password:123456	fullname:nn	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:5	', '2014-08-27 17:00:00'),
(40, 108, 'Thông tin cũ: idusers:108	username:noname	email:no@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:nn	birthday:0000-00-00	address:	<br/>Thông tin mới: username:noname	address:hn	birthday:--0000-00-00	email:no@gmail.com	password:123456	fullname:nn	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:5	', '2014-08-27 17:00:00'),
(41, 108, 'Thông tin cũ: idusers:108	username:noname	email:no@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:nn	birthday:0000-00-00	address:hn	<br/>Thông tin mới: username:noname	address:hn	birthday:--0000-00-00	email:no@gmail.com	password:123456	fullname:nn	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:1	', '2014-08-27 17:00:00'),
(42, 108, 'Thông tin cũ: idusers:108	username:noname	email:no@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:nn	birthday:0000-00-00	address:hn	<br/>Thông tin mới: username:noname	address:hnv	birthday:--0000-00-00	email:no@gmail.com	password:123456	fullname:nn	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:4	', '2014-08-27 17:00:00'),
(43, 108, 'Thông tin cũ: idusers:108	username:noname	email:no@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:nn	birthday:0000-00-00	address:hnv	<br/>Thông tin mới: username:noname	address:hnv	birthday:--0000-00-00	email:no@gmail.com	password:123456	fullname:nn	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:3	', '2014-08-27 17:00:00'),
(44, 108, 'Thông tin cũ: idusers:108	username:noname	email:no@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:nn	birthday:0000-00-00	address:hnv	<br/>Thông tin mới: username:noname	address:hnv	birthday:--0000-00-00	email:no@gmail.com	password:123456	fullname:nn	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:3	', '2014-08-27 17:00:00'),
(45, 108, 'Thông tin cũ: idusers:108	username:noname	email:no@gmail.com	password:123456	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	fullname:nn	birthday:0000-00-00	address:hnv	<br/>Thông tin mới: username:noname	address:hnvv	birthday:--0000-00-00	email:no@gmail.com	password:123456	fullname:nn	phone:01934719893	start_job:2014-08-08	end_job:0000-00-00	idroom:5	', '2014-08-27 17:00:00'),
(47, 125, '<p class = ''old''>Thông tin cũ: idusers:125<br/>username:buon<br/>email:buon@gmail.co<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:0000-00-00<br/>address:buon@gmail.co<br/>start_job:2014-09-24<br/>end_job:0000-00-00<br/></p><p class = "new">Thông tin mới: username:buonbuon<br/>address:buon@gmail.co<br/>birthday:--0000-00-00<br/>email:buon@gmail.co<br/>password:123456<br/>fullname:no name<br/>phone:01689988291<br/>start_job:--2014-09-24<br/>end_job:--0000-00-00<br/></p>', '2014-09-02 17:00:00'),
(48, 125, '<p class = ''old''>Thông tin cũ: idusers:125<br/>username:buonbuon<br/>email:buon@gmail.co<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:0000-00-00<br/>address:buon@gmail.co<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/></p><p class = "new">Thông tin mới: iduser: 125 <br/>username:buonbuon<br/>address:buon@gmail.commmm<br/>birthday:2014-09-04<br/>email:buon@gmail.commmm<br/>password:123456<br/>fullname:no nameeeeeee<br/>phone:01689988291<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-02 17:00:00'),
(56, 125, '<p class = ''old''>Thông tin cũ: idusers:125<br/>username:buonbuon<br/>email:buon@gmail.commmm<br/>password:123456<br/>phone:01689988291<br/>fullname:no nameeeeeee<br/>birthday:0000-00-00<br/>address:buon@gmail.commmm<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/125/5-3.jpg<br/></p><p class = "new">Thông tin mới: iduser: 125 <br/>username:buonbuon<br/>email:buon@gmail.commmm<br/>password:123456<br/>phone:01689988291<br/>fullname:no nameeeeeee<br/>birthday:--0000-00-00<br/>address:buon@gmail.commmm<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-03 17:00:00'),
(59, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(60, 121, 'linh tinh', '2014-09-04 17:00:00'),
(61, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(64, 126, '<p class = ''old''>Thông tin cũ: idusers:126<br/>username:hihihehe<br/>email:chan@gmai.com<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:0000-00-00<br/>address:thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/126/20140304-0139-phu-nu-1.jpg<br/></p><p class = "new">Thông tin mới: iduser: 126 <br/>username:hihihehe<br/>email:chan@gmai.com<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:--0000-00-00<br/>address:thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(65, 125, '<p class = ''old''>Thông tin cũ: idusers:125<br/>username:buonbuon<br/>email:buon@gmail.commmm<br/>password:123456<br/>phone:01689988291<br/>fullname:no nameeeeeee<br/>birthday:0000-00-00<br/>address:buon@gmail.commmm<br/>start_job:2014-09-04<br/>end_job:0000-00-00<br/>avatar:/upload/125/5-3.jpg<br/></p><p class = "new">Thông tin mới: iduser: 125 <br/>username:buonbuon<br/>email:buon@gmail.commmm<br/>password:123456<br/>phone:01689988291<br/>fullname:no nameeeeeee<br/>birthday:--0000-00-00<br/>address:buon@gmail.commmm<br/>start_job:--2014-09-04<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(66, 125, '<p class = ''old''>Thông tin cũ: idusers:125<br/>username:buonbuon<br/>email:buon@gmail.commmm<br/>password:123456<br/>phone:01689988291<br/>fullname:no nameeeeeee<br/>birthday:0000-00-00<br/>address:buon@gmail.commmm<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/125/5-3.jpg<br/></p><p class = "new">Thông tin mới: iduser: 125 <br/>username:buonbuon<br/>email:buon@gmail.commmm<br/>password:123456<br/>phone:01689988291<br/>fullname:no nameeeeeee<br/>birthday:--0000-00-00<br/>address:buon@gmail.commmm<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(67, 124, '<p class = ''old''>Thông tin cũ: idusers:124<br/>username:bb<br/>email:bb@gmail.com<br/>password:123456<br/>phone:02948948989<br/>fullname:no name<br/>birthday:0000-00-00<br/>address:<br/>start_job:0000-00-00<br/>end_job:2014-09-04<br/>avatar:upload/125/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 124 <br/>username:bb<br/>email:bb@gmail.com<br/>password:123456<br/>phone:02948948989<br/>fullname:no name<br/>birthday:--0000-00-00<br/>address:<br/>start_job:--0000-00-00<br/>end_job:--2014-09-04<br/></p>', '2014-09-04 17:00:00'),
(68, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(69, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(70, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(71, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(72, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(73, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(74, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(75, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(76, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(77, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(78, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(79, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(80, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(81, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(82, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(83, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(84, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(85, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(86, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(87, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(88, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(89, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(90, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(91, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(92, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(93, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(94, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(95, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(96, 121, '<p class = ''old''>Thông tin cũ: idusers:121<br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/121/20140304-0139-phu-nu-1.jpg<br/></p><p class = "new">Thông tin mới: iduser: 121 <br/>username:bunmotchuthoi<br/>email:dunglo@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:trangnt<br/>birthday:--0000-00-00<br/>address:van cam hung ha thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(97, 126, '<p class = ''old''>Thông tin cũ: idusers:126<br/>username:hihihehe<br/>email:chan@gmai.com<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:0000-00-00<br/>address:thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/126/20140304-0139-phu-nu-1.jpg<br/></p><p class = "new">Thông tin mới: iduser: 126 <br/>username:hihihehe<br/>email:chan@gmai.com<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:--0000-00-00<br/>address:thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(103, 126, '<p class = ''old''>Thông tin cũ: idusers:126<br/>username:hihihehe<br/>email:chan@gmai.com<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:0000-00-00<br/>address:thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/126/20140304-0139-phu-nu-1.jpg<br/></p><p class = "new">Thông tin mới: iduser: 126 <br/>username:hihihehe<br/>email:chan@gmai.com<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:--0000-00-00<br/>address:thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(104, 123, '<p class = ''old''>Thông tin cũ: idusers:123<br/>username:sdsdsds<br/>email:sdsd@sfadad<br/>password:123456<br/>phone:01934719893<br/>fullname:sádsadsadsad<br/>birthday:0000-00-00<br/>address:<br/>start_job:2014-09-05<br/>end_job:0000-00-00<br/>avatar:upload/125/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 123 <br/>username:sdsdsds<br/>email:sdsd@sfadad<br/>password:123456<br/>phone:01934719893<br/>fullname:sádsadsadsad<br/>birthday:--0000-00-00<br/>address:<br/>start_job:--2014-09-05<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(105, 123, '<p class = ''old''>Thông tin cũ: idusers:123<br/>username:sdsdsds<br/>email:sdsd@sfadad<br/>password:123456<br/>phone:01934719893<br/>fullname:sádsadsadsad<br/>birthday:0000-00-00<br/>address:<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:upload/125/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 123 <br/>username:sdsdsds<br/>email:sdsd@gmail<br/>password:123456<br/>phone:01934719893<br/>fullname:sádsadsadsad<br/>birthday:--0000-00-00<br/>address:<br/>start_job:--2014-09-05<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(106, 123, '<p class = ''old''>Thông tin cũ: idusers:123<br/>username:sdsdsds<br/>email:sdsd@gmail<br/>password:123456<br/>phone:01934719893<br/>fullname:sádsadsadsad<br/>birthday:0000-00-00<br/>address:<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:upload/125/15-2.jpg<br/></p><p class = "new">Thông tin mới: iduser: 123 <br/>username:sdsdsds<br/>email:oitroi@gmail.com<br/>password:123456<br/>phone:01934719893<br/>fullname:sádsadsadsad<br/>birthday:--0000-00-00<br/>address:<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(107, 127, '<p class = ''old''>Thông tin cũ: idusers:127<br/>username:memmer<br/>email:member@gamil.com<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:2013-07-22<br/>address:van cam - hung ha - thai binh<br/>start_job:2014-09-05<br/>end_job:<br/>avatar:<br/></p><p class = "new">Thông tin mới: iduser: 127 <br/>username:memmer<br/>email:member@gamil.com<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:--2013-07-22<br/>address:van cam - hung ha - thai binh<br/>start_job:--2014-09-05<br/>end_job:--<br/></p>', '2014-09-04 17:00:00'),
(108, 127, '<p class = ''old''>Thông tin cũ: idusers:127<br/>username:memmer<br/>email:member@gamil.com<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:0000-00-00<br/>address:van cam - hung ha - thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/1/5-3.jpg<br/></p><p class = "new">Thông tin mới: iduser: 127 <br/>username:memmer<br/>email:member@gamil.com<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:--0000-00-00<br/>address:van cam - hung ha - thai binh<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(109, 126, '<p class = ''old''>Thông tin cũ: idusers:126<br/>username:hihihehe<br/>email:chan@gmai.com<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:0000-00-00<br/>address:thai binh<br/>start_job:2014-09-05<br/>end_job:0000-00-00<br/>avatar:/upload/126/20140304-0139-phu-nu-1.jpg<br/></p><p class = "new">Thông tin mới: iduser: 126 <br/>username:hihihehe<br/>email:chan@gmai.com<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:--0000-00-00<br/>address:thai binh<br/>start_job:--2014-09-05<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(110, 126, '<p class = ''old''>Thông tin cũ: idusers:126<br/>username:hihihehe<br/>email:chan@gmai.com<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:0000-00-00<br/>address:thai binh<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/126/20140304-0139-phu-nu-1.jpg<br/></p><p class = "new">Thông tin mới: iduser: 126 <br/>username:hihihehe<br/>email:chan@gmai.com<br/>password:123456<br/>phone:01689988291<br/>fullname:no name<br/>birthday:--0000-00-00<br/>address:thai binh<br/>start_job:--2014-09-05<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00'),
(111, 125, '<p class = ''old''>Thông tin cũ: idusers:125<br/>username:buonbuon<br/>email:buon@gmail.commmm<br/>password:123456<br/>phone:01689988291<br/>fullname:no nameeeeeee<br/>birthday:0000-00-00<br/>address:buon@gmail.commmm<br/>start_job:0000-00-00<br/>end_job:0000-00-00<br/>avatar:/upload/125/5-3.jpg<br/></p><p class = "new">Thông tin mới: iduser: 125 <br/>username:buonbuon<br/>email:buon@gmail.commmm<br/>password:123456<br/>phone:01689988291<br/>fullname:no nameeeeeee<br/>birthday:--0000-00-00<br/>address:buon@gmail.commmm<br/>start_job:--0000-00-00<br/>end_job:--0000-00-00<br/></p>', '2014-09-04 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `idpost` int(11) NOT NULL AUTO_INCREMENT,
  `idusers` int(11) NOT NULL,
  `idcategory` int(11) DEFAULT NULL,
  `detail` longtext,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`idpost`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`idpost`, `idusers`, `idcategory`, `detail`, `start_date`, `end_date`) VALUES
(1, 1, 2, 'lt', '2014-08-05', '0000-00-00'),
(2, 1, 2, 'test', '2014-08-05', '0000-00-00'),
(3, 1, 3, '33', '2014-08-05', '0000-00-00'),
(4, 1, 3, 'aaaaaaaaaa', '2014-08-05', '1970-01-01'),
(5, 61, 3, 'dealine', '2014-08-16', '1970-01-01'),
(6, 76, 1, 'kinh te ngheo nan lac hau', '2014-08-19', '1970-01-01'),
(7, 61, 1, 'test 2', '2014-09-05', '1970-01-01');

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE IF NOT EXISTS `room` (
  `idroom` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(32) NOT NULL,
  PRIMARY KEY (`idroom`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`idroom`, `name`, `phone`) VALUES
(0, 'chờ duyệt', ''),
(1, 'giám đốcs', '1234567890'),
(2, 'phòng game', '016899882191'),
(3, 'phòng web', '01689988291');

-- --------------------------------------------------------

--
-- Table structure for table `roomuser`
--

CREATE TABLE IF NOT EXISTS `roomuser` (
  `idlevel` int(11) NOT NULL COMMENT 'phong nao',
  `iduser` int(11) NOT NULL,
  `chucvu` varchar(15) NOT NULL,
  PRIMARY KEY (`chucvu`,`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roomuser`
--

INSERT INTO `roomuser` (`idlevel`, `iduser`, `chucvu`) VALUES
(3, 127, '0'),
(4, 121, '2'),
(5, 124, '2'),
(4, 121, '4'),
(1, 125, '4'),
(3, 126, '4');

-- --------------------------------------------------------

--
-- Table structure for table `rule`
--

CREATE TABLE IF NOT EXISTS `rule` (
  `idrole` int(11) NOT NULL AUTO_INCREMENT,
  `idgroup` int(11) NOT NULL,
  `resource` varchar(45) NOT NULL,
  `action` varchar(255) NOT NULL,
  PRIMARY KEY (`idrole`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `rule`
--

INSERT INTO `rule` (`idrole`, `idgroup`, `resource`, `action`) VALUES
(1, 2, 'post', 'index,list'),
(2, 2, 'category', 'index,insert,edit,delete,post'),
(3, 3, 'users', 'index,list,detail,delete,insert'),
(5, 2, 'users', 'index,list,detail,getTime'),
(6, 1, 'users', 'index,list,detail,getTime'),
(7, 1, 'post', 'index,list');

-- --------------------------------------------------------

--
-- Table structure for table `useratribute`
--

CREATE TABLE IF NOT EXISTS `useratribute` (
  `iduserAtribute` int(11) NOT NULL AUTO_INCREMENT,
  `idusers` int(11) NOT NULL,
  `keyu` int(11) NOT NULL,
  `valueu` varchar(255) NOT NULL,
  PRIMARY KEY (`iduserAtribute`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=209 ;

--
-- Dumping data for table `useratribute`
--

INSERT INTO `useratribute` (`iduserAtribute`, `idusers`, `keyu`, `valueu`) VALUES
(199, 61, 9, 'test1,test2'),
(200, 61, 8, 'listx.phtml,cdbc.dll,sqlite3.dll'),
(208, 76, 1, '477a85phunutodaydanhmuc3.jpg,477a85phunutodaydanhmuc3.jpg,477a85phunutodaydanhmuc3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `idusers` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `fullname` varchar(45) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `start_job` date NOT NULL,
  `end_job` date DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idusers`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=128 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idusers`, `username`, `email`, `password`, `phone`, `fullname`, `birthday`, `address`, `start_job`, `end_job`, `avatar`) VALUES
(61, 'admin', 'admin@gmail.com', '123456', '1234567890', 'adsa', '0000-00-00', '', '0000-00-00', '0000-00-00', 'upload/125/15-2.jpg'),
(124, 'bb', 'bb@gmail.com', '123456', '02948948989', 'no name', '0000-00-00', '', '2014-09-05', '0000-00-00', 'upload/125/15-2.jpg'),
(125, 'buonbuon', 'buon@gmail.commmm', '123456', '01689988291', 'no nameeeeeee', '0000-00-00', 'buon@gmail.commmm', '0000-00-00', '0000-00-00', '/upload/125/5-3.jpg'),
(126, 'hihihehe', 'chan@gmai.com', '123456', '01689988291', 'no name', '0000-00-00', 'thai binh', '0000-00-00', '0000-00-00', '/upload/126/20140304-0139-phu-nu-1.jpg'),
(127, 'memmer', 'member@gamil.com', '123456', '01689988291', 'no name', '0000-00-00', 'van cam - hung ha - thai binh', '0000-00-00', '0000-00-00', '/upload/127/5-3.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
